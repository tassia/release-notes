# about.po translation to Spanish
# Copyright (C) 2009-2019 Free Software Foundation, Inc.
# This file is distributed under the same license as the release-notes.
#
# Copyright (C) Translators and reviewers (see below) of the Debian Spanish translation team
# - Translators:
#       Ricardo Cárdenes Medina
#       Juan Manuel García Molina
#       Javier Fernández-Sanguino Peña
#       David Martínez Moreno
#       Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2009
# - Reviewers:
#        Martín Ferrari, 2007
#	 Javier <jac@inventati.org>, 2019
#
# Changes:
#   - Updated for Lenny
#       Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2009
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2009
#   - Updated for Squeeze
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2010-2011
#   - Updated for Wheezy
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2013
#   - Updated for Jessie
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2015
#   - Updated for Stretch
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2017
#   - Updated for Buster
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2019
#       
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
#       info -n '(gettext)PO Files'
#       info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
#   - El proyecto de traducción de Debian al español
#     http://www.debian.org/intl/spanish/
#     especialmente las notas de traducción en
#     http://www.debian.org/intl/spanish/notas
#
#   - La guía de traducción de po's de debconf:
#     /usr/share/doc/po-debconf/README-trans
#     o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2019-04-09 15:27+0200\n"
"PO-Revision-Date: 2019-07-05 00:13+0200\n"
"Last-Translator:  Javier Fernández-Sanguino <jfs@debian.org>\n"
"Language-Team: Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: xsltproc apt vs aptitude dblatex backups upgrade lib\n"
"X-POFile-SpellExtra: script DocBook var status get pkgstates release PDF\n"
"X-POFile-SpellExtra: dpkg log term reports xmlroff docbook gzip xsl\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "es"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Introducción"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Este documento informa a los usuarios de la distribución &debian; sobre los "
"cambios más importantes de la versión &release; (nombre en clave "
"«&releasename;»)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"Las notas de publicación proporcionan la información sobre cómo actualizar "
"de una forma segura desde la versión &oldrelease; (nombre en clave "
"«&oldreleasename;») a la versión actual e informan a los usuarios sobre los "
"problemas conocidos que podrían encontrarse durante este proceso."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>.  If in doubt, check the date on the first page to "
"make sure you are reading a current version."
msgstr ""
"Puede obtener la versión más reciente de este documento en <ulink url=\"&url-"
"release-notes;\"></ulink>. Si tiene alguna duda, compruebe la fecha en la "
"primera página para asegurarse que está Vd. leyendo la última versión."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:27
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Tenga en cuenta que es imposible hacer una lista con todos los posibles "
"problemas conocidos y que, por tanto, se ha hecho una selección de los "
"problemas más relevantes basándose en una combinación de la frecuencia con "
"la que pueden aparecer y su impacto en el proceso de actualización."

#. type: Content of: <chapter><para>
#: en/about.dbk:33
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Tenga en cuenta que solo se da soporte y se documenta la actualización desde "
"la versión anterior de Debian (en este caso, la actualización desde "
"«&oldreleasename;»). Si necesita actualizar su sistema desde una versión más "
"antigua, le sugerimos que primero actualice a la versión &oldreleasename; "
"consultando las ediciones anteriores de las notas de publicación."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:41
msgid "Reporting bugs on this document"
msgstr "Cómo informar de fallos en este documento"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:43
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Hemos intentado probar todos los posibles pasos de actualización descritos "
"en este documento y anticipar todos los problemas posibles con los que un "
"usuario podría encontrarse."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:48
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"En cualquier caso, si piensa que ha encontrado una errata en esta documento, "
"mande un informe de error (en inglés) al <ulink url=\"&url-bts;\">sistema de "
"seguimiento de fallos</ulink> contra el paquete <systemitem role=\"package"
"\">release-notes</systemitem>. Puede que desee revisar primero los <ulink "
"url=\"&url-bts-rn;\">informes de erratas existentes</ulink> para ver si el "
"problema que Vd. ha encontrado ya se ha reportado. Siéntase libre de añadir "
"información adicional a informes de erratas existentes si puede ayudar a "
"mejorar este documento."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:60
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Apreciamos y le animamos a que nos envíe informes incluyendo parches a las "
"fuentes del documento. Puede encontrar más información describiendo cómo "
"obtener las fuentes de este documento en <xref linkend=\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:68
msgid "Contributing upgrade reports"
msgstr "Cómo contribuir con informes de actualización"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:70
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Agradecemos cualquier información que los usuarios quieran proporcionar "
"relacionada con las actualizaciones desde la versión &oldreleasename; a la "
"versión &releasename;. Si está dispuesto a compartir la información, por "
"favor mande un informe de fallo al <ulink url=\"&url-bts;\">sistema de "
"seguimiento de fallos</ulink>. Utilice para el informe el paquete "
"<systemitem role=\"package\">upgrade-reports</systemitem> y envíenos el "
"resultado de su actualización. Por favor, comprima cualquier archivo adjunto "
"que incluya (utilizando <command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:79
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Le agradeceríamos que incluyera la siguiente información cuando envíe su "
"informe de actualización:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:86
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"El estado de su base de datos de paquetes antes y después de la "
"actualización: la base de datos del estado de <systemitem role=\"package"
"\">dpkg</systemitem> (disponible en el archivo <filename>/var/lib/dpkg/"
"status</filename>) y la información del estado de los paquetes de "
"<systemitem role=\"package\">apt</systemitem> (disponible en el archivo "
"<filename>/var/lib/apt/extended_states</filename>). Debería realizar una "
"copia de seguridad de esta información antes de hacer la actualización, tal "
"y como se describe en <xref linkend=\"data-backup\"/>, aunque también puede "
"encontrar copias de seguridad de <filename>/var/lib/dpkg/status</filename> "
"en el directorio <filename>/var/backups</filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:99
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Los registros de la sesión que haya creado al utilizar <command>script</"
"command>, tal y como se describe en <xref linkend=\"record-session\"/>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:105
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Sus registros de <systemitem role=\"package\">apt</systemitem>, disponibles "
"en el archivo <filename>/var/log/apt/term.log</filename>, o sus registros de "
"<command>aptitude</command>, disponibles en el archivo <filename>/var/log/"
"aptitude</filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:114
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Debería dedicar algún tiempo a revisar y eliminar cualquier información "
"sensible o confidencial de los registros antes de incluirlos dentro de un "
"informe de fallo ya que la información enviada se incluirá en una base de "
"datos pública."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:123
msgid "Sources for this document"
msgstr "Fuentes de este documento"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:125
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr "Los archivos fuentes de este documento están en formato DocBook XML<indexterm><primary>DocBook XML</primary></indexterm>. La versión HTML se generó utilizando <systemitem role=\"package\">docbook-xsl</systemitem> y <systemitem role=\"package\">xsltproc</systemitem>. La versión PDF se generó utilizando <systemitem role=\"package\">dblatex</systemitem> o <systemitem role=\"package\">xmlroff</systemitem>. Los ficheros fuente de las notas de publicación están disponibles en el repositorio de Git del <emphasis>Proyecto de Documentación de Debian</emphasis>. Puede utilizar la <ulink url=\"&url-vcs-release-notes;\">interfaz web</ulink> para acceder de forma individual a los archivos y consultar los cambios realizados. Consulte las <ulink url=\"&url-ddp-vcs-info;\">páginas de información de Git del Proyecto de Documentación de Debian</ulink> para más información sobre cómo acceder al repositorio de fuentes."

#~ msgid "TODO: any more things to add here?\n"
#~ msgstr "TODO: ¿añadir más cosas aquí?\n"

#~ msgid ""
#~ "\n"
#~ "TODO: check status of #494028 about apt-get vs. aptitude\n"
#~ "TODO: any more things to add here?\n"
#~ msgstr ""
#~ "\n"
#~ "POR HACER: comprobar el estado de #494028 sobre apt-get vs. aptitude\n"
#~ "POR HACER: ¿algo más que añadir aquí?\n"

#~ msgid "The source of this document is in DocBook XML<indexterm>"
#~ msgstr ""
#~ "Los fuentes de este documento están en formato DocBook XML<indexterm>"

#~ msgid "DocBook XML"
#~ msgstr "DocBook XML"
