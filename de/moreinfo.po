# German translation of the Debian release notes
#
# Jan Hauke Rahm <info@jhr-online.de>, 2009.
# Holger Wansing <linux@wansing-online.de>, 2011, 2013, 2015, 2017.
# Holger Wansing <hwansing@mailbox.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 9.0\n"
"POT-Creation-Date: 2019-03-16 21:22+0100\n"
"PO-Revision-Date: 2019-03-16 22:30+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "de"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Zusätzliche Informationen zu &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Weitere Lektüre"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Neben diesen Hinweisen zur Veröffentlichung und der Installationsanleitung "
"sind weitere Informationen zu Debian beim Debian-Dokumentationsprojekt (DDP) "
"erhältlich, dessen Ziel es ist, hochwertige Dokumentation für Debian-"
"Anwender und -Entwickler zu erstellen. Dazu gehören die Debian-Referenz, der "
"Debian-Leitfaden für neue Paketbetreuer, die häufig gestellten Fragen zu "
"Debian (Debian-FAQ) und viele weitere. Bezüglich genauer Details über die "
"zur Verfügung stehenden Dokumente sehen Sie auf der <ulink url=\"&url-ddp;"
"\">Debian-Dokumentations-Website</ulink> und im <ulink url=\"&url-wiki;"
"\">Debian Wiki</ulink> nach."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Dokumentation zu einzelnen Paketen ist in <filename>/usr/share/doc/"
"<replaceable>Paket</replaceable></filename> installiert. Das schließt "
"Urheberrechtsinformationen, Debian-spezifische Details und Dokumentation der "
"Original-Autoren ein."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Hilfe bekommen"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Es gibt viele Quellen für Hilfe, Ratschläge und Unterstützung für Debian-"
"Anwender, aber sie sollten möglichst nur in Betracht gezogen werden, wenn "
"Sie die vorhandene Dokumentation nach Lösungen für Ihr Problem durchsucht "
"haben. Dieser Abschnitt gibt eine kurze Einführung zu diesen Quellen, die "
"besonders für neue Debian-Anwender hilfreich sein werden."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Mailinglisten"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Die für Debian-Anwender interessantesten Mailinglisten sind <quote>debian-"
"user</quote> (Englisch) und weitere, wie debian-user-<replaceable>sprache</"
"replaceable> (für verschiedene Sprachen, bspw. debian-user-german). Weitere "
"Informationen zu den Listen und wie diese abonniert werden können, sind auf "
"den Seiten der <ulink url=\"&url-debian-list-archives;\">Debian-"
"Mailinglisten</ulink> beschrieben. Bitte suchen Sie vor dem Schreiben erst "
"in den Listenarchiven nach bereits gegebenen Antworten und bitte beachten "
"Sie auch die Etikette für die Kommunikation auf Mailinglisten."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian hat einen IRC-Kanal im OFTC-IRC-Netzwerk, der für die Unterstützung "
"von Debian-Anwendern bestimmt ist. Um in diesen Kanal zu gelangen, verbinden "
"Sie Ihr IRC-Programm mit irc.debian.org und verwenden Sie den Kanal "
"<literal>#debian</literal> (englisch)."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Bitte beachten Sie die Leitsätze im Umgang mit dem Kanal und respektieren "
"Sie die anderen Benutzer. Die Leitsätze finden Sie im <ulink url=\"&url-wiki;"
"DebianIRC\">Debian Wiki</ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Für weitere Informationen zum OFTC besuchen Sie bitte dessen <ulink url="
"\"&url-irc-host;\">Website</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Fehler berichten"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Wir bemühen uns, Debian zu einem hochqualitativen Betriebssystem zu machen. "
"Das bedeutet aber nicht, dass alle Pakete, die wir zur Verfügung stellen, "
"fehlerfrei sind. Übereinstimmend mit Debians Philosophie der <quote>offenen "
"Entwicklung</quote> und als Service für unsere Anwender stellen wir alle "
"Informationen zu gemeldeten Fehlern in unserer Fehlerdatenbank (Bug Tracking "
"System, BTS) bereit. Dieses BTS können Sie unter <ulink url=\"&url-bts;\"></"
"ulink> durchsuchen."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Falls Sie einen Fehler in der Distribution oder einem darin enthaltenen "
"Paket finden, berichten Sie den Fehler bitte, sodass er für weitere "
"Veröffentlichungen ordentlich behoben werden kann. Um Fehler zu berichten, "
"ist eine gültige E-Mail-Adresse nötig. Wir bitten darum, damit wir Fehler "
"verfolgen und die Entwickler Kontakt zu denjenigen aufnehmen können, die den "
"Fehler berichtet haben, wenn weitere Informationen dazu benötigt werden."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Sie können einen Fehler mit Hilfe des Programms <command>reportbug</command> "
"oder manuell per E-Mail berichten. Weitere Informationen zum "
"Fehlerdatenbanksystem und wie es zu bedienen ist finden Sie in der "
"Referenzdokumentation (unter <filename>/usr/share/doc/debian</filename>, "
"wenn Sie <systemitem role=\"package\">doc-debian</systemitem> installiert "
"haben) oder online in der <ulink url=\"&url-bts;\">Fehlerdatenbank</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Zu Debian beitragen"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Sie müssen kein Experte sein, um etwas zu Debian beitragen zu können. Sie "
"unterstützen die Gemeinschaft beispielsweise, indem Sie bei den "
"verschiedenen Benutzeranfragen in den <ulink url=\"&url-debian-list-archives;"
"\">User-Mailinglisten</ulink> helfen. Fehler im Zusammenhang mit der "
"Entwicklung der Distribution zu finden (und zu beheben), indem Sie sich in "
"den <ulink url=\"&url-debian-list-archives;\">Entwickler-Mailinglisten</"
"ulink> einbringen, ist ebenfalls sehr hilfreich. Sie helfen Debians "
"hochqualitativer Distribution auch, indem Sie <ulink url=\"&url-bts;"
"\">Fehler berichten</ulink> und die Entwicklern dabei unterstützen, diese "
"genauer zu identifizieren und zu lösen. Das Programm <systemitem role="
"\"package\">how-can-i-help</systemitem> hilft Ihnen dabei, passende "
"Fehlerberichte zu finden, an denen Sie arbeiten können. Falls Sie gut im "
"Umgang mit Worten sind, können Sie auch helfen, <ulink url=\"&url-ddp-vcs-"
"info;\">Dokumentation</ulink> zu schreiben oder bereits bestehende "
"Dokumentation in Ihre eigene Sprache zu <ulink url=\"&url-debian-i18n;"
"\">übersetzen</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Falls Sie mehr Zeit zur Verfügung haben, könnten Sie auch einen Teil der "
"Freien Software in Debian verwalten. Besonders hilfreich ist es, wenn Teile "
"übernommen werden, für die darum gebeten wurde, sie Debian zu hinzuzufügen. "
"Die <ulink url=\"&url-wnpp;\">Datenbank der Arbeit bedürfenden Pakete "
"(WNPP)</ulink> gibt dazu detaillierte Informationen. Falls Sie Interesse an "
"bestimmten Anwendergruppen haben, finden Sie vielleicht Freude daran, etwas "
"zu einzelnen <ulink url=\"&url-debian-projects;\">Unterprojekten</ulink> von "
"Debian beizutragen, wie beispielsweise zur Portierung auf andere "
"Architekturen und zu <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> (angepasste Debian-Distributionen)."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"Ob Sie nun als Anwender, Programmierer, Autor oder Übersetzer in der "
"Gemeinschaft der Freien Software arbeiten, Sie helfen auf jeden Fall den "
"Bemühungen der Freie-Software-Bewegung. Mitzuhelfen macht Spaß und honoriert "
"die Arbeit anderer, und genauso wie es Ihnen ermöglicht, neue Leute kennen "
"zu lernen, gibt es Ihnen auch dieses unbestimmte, schöne Gefühl, dabei zu "
"sein."
