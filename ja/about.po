# Japanese translations for Debian release notes
# Debian リリースノートの日本語訳
# Noritada Kobayashi <nori1@dolphin.c.u-tokyo.ac.jp>, 2006.
# KURASAWA Nozomu <nabetaro@caldron.jp>, 2009.
# Hideki Yamane <henrich@debian.org>, 2010, 2013, 2015-2019.
# hoxp18 <hoxp18@noramail.jp>, 2019.
# This file is distributed under the same license as Debian release notes.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 9\n"
"Report-Msgid-Bugs-To: debian-doc@lists.debian.org\n"
"POT-Creation-Date: 2019-06-01 13:06+0900\n"
"PO-Revision-Date: 2019-06-15 17:26+0900\n"
"Last-Translator: Hideki Yamane <henrich@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "ja"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "はじめに"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"この文書は &debian; ディストリビューションのユーザーに、バージョン &release; "
"(コードネーム &releasename;) での大きな変更点を知らせるものです。"

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"このリリースノートでは、リリース &oldrelease; (コードネーム "
"&oldreleasename;) から今回のリリースへの安全なアップグレード方法や、その際"
"ユーザーが遭遇する可能性がある既知の問題点についての情報をユーザーに提供していま"
"す。"

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>.  If in doubt, check the date on the first page to "
"make sure you are reading a current version."
msgstr ""
"この文書の最新版は、<ulink url=\"&url-release-notes;\"></ulink> から取得でき"
"ます。疑わしい場合は、最初のページにある文書の日付をチェックし、最新版を読ん"
"でいるかを確認してください。"

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:27
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"既知の問題点をすべてリストアップすることは不可能なので、問題点の予想される広"
"がり具合と影響の大きさの両方に基づいて取捨選択していることに注意してくださ"
"い。"

#. type: Content of: <chapter><para>
#: en/about.dbk:33
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Debian の 1 つ前のリリースからのアップグレード (この場合、&oldreleasename; か"
"らのアップグレード) のみがサポート・記述されていることに注意してください。さ"
"らに古いリリースからのアップグレードが必要な場合は、過去のリリースノートを読"
"み、まず &oldreleasename; へとアップグレードすることをお勧めします。"

#. type: Content of: <chapter><section><title>
#: en/about.dbk:41
msgid "Reporting bugs on this document"
msgstr "この文書に関するバグを報告する"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:43
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"私たちは、この文書で説明されているすべての異なるアップグレード手順を試し、ま"
"た、ユーザーが直面する可能性のある、すべての問題を想定しました。"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:48
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"それにもかかわらず、この文書にバグ (不正確な情報や抜け落ちている情報) を見つ"
"けたと思う場合には、<systemitem role=\"package\">release-notes</systemitem> "
"パッケージに対するバグ報告として、<ulink url=\"&url-bts;\">バグ追跡システム</"
"ulink>に提出してください。あなたが発見した問題が既に報告されている場合に備"
"え、まずは<ulink url=\"&url-bts-rn;\">既存のバグ報告</ulink>を確認してみると"
"良いでしょう。もしこの文書にさらに内容を付加できるのであれば、どうぞ遠慮なく"
"既存のバグ報告へ情報を追加して下さい。"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:60
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"私たちは、この文書のソースへのパッチを含めた報告を歓迎・推奨します。このド"
"キュメントのソースの取得方法の記述については <xref linkend=\"sources\"/> で、"
"より詳細な情報を見つけることができるでしょう。"

#. type: Content of: <chapter><section><title>
#: en/about.dbk:68
msgid "Contributing upgrade reports"
msgstr "アップグレードについての報告をする"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:70
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"&oldreleasename; から &releasename; へのアップグレードに関連するユーザーからの"
"情報はどんなものでも歓迎します。情報を共有するのを厭わない場合は、"
"<systemitem role=\"package\">upgrade-reports</systemitem> パッケージに対する"
"バグ報告として、アップグレードの結果を含めて<ulink url=\"&url-bts;\">バグ追跡"
"システム</ulink>に提出してください。報告に添付ファイルを含める場合は、"
"(<command>gzip</command> を使用して) 圧縮するようお願いします。"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:79
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"アップグレードについての報告を提出する際には、以下の情報を含めてください。"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:86
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"アップグレード前後のパッケージデータベースの状態。<filename>/var/lib/dpkg/"
"status</filename> にある <systemitem role=\"package\">dpkg</systemitem> の状"
"態データベースと、<filename>/var/lib/apt/extended_states</filename> にある "
"<systemitem role=\"package\">apt</systemitem> のパッケージ状態情報です。"
"<xref linkend=\"data-backup\"/> で説明するように、アップグレードを実行する前"
"にバックアップをとっておくべきですが、<filename>/var/lib/dpkg/status</"
"filename> のバックアップは <filename>/var/backups</filename> にもあります。"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:99
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"<command>script</command> を使用して作成したセッションのログ。<xref linkend="
"\"record-session\"/> で説明します。"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:105
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"<filename>/var/log/apt/term.log</filename> にある <systemitem role=\"package"
"\">apt</systemitem> のログか、<filename>/var/log/aptitude</filename> にある "
"<command>aptitude</command> のログ。"

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:114
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"バグ報告に情報を含める前に、慎重に扱うべき情報や機密情報がログに含まれていな"
"いかある程度時間をかけて検査し、ログから削除してください。なぜなら、バグ報告"
"に含まれる情報は公開データベースで公表されるからです。"

#. type: Content of: <chapter><section><title>
#: en/about.dbk:123
msgid "Sources for this document"
msgstr "この文書のソース"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:125
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"この文書のソースは DocBook XML<indexterm><primary>DocBook XML</primary></"
"indexterm> 形式です。HTML 版は、<systemitem role=\"package\">docbook-xsl</"
"systemitem> と <systemitem role=\"package\">xsltproc</systemitem> を使用して"
"生成しています。PDF 版は、<systemitem role=\"package\">dblatex</systemitem> "
"や <systemitem role=\"package\">xmlroff</systemitem> を使用して生成していま"
"す。リリースノートのソースは <emphasis>Debian ドキュメンテーションプロジェク"
"ト (Debian Documentation Project)</emphasis> の Git リポジトリにあります。"
"ウェブから<ulink url=\"&url-vcs-release-notes;\">ウェブインターフェース</"
"ulink>を使って個々のファイルにアクセスでき、変更を参照できます。Git へのアク"
"セス方法に関してさらに詳しく知りたい場合は、<ulink url=\"&url-ddp-vcs-info;"
"\">Debian ドキュメンテーションプロジェクトの VCS 情報ページ</ulink>を参照して"
"ください。"
